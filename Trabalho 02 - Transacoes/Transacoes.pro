QT += core
QT -= gui

CONFIG += c++11

TARGET = Transacoes
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    state.cpp \
    transaction.cpp \
    controller.cpp

HEADERS += \
    general.h \
    state.h \
    transaction.h \
    controller.h
